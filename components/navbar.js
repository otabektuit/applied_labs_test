import React from 'react'

function Navbar(){
    return (
        <nav className="nav">
            <div className="nav_discount">
                <div className="container">
                    <span className="discount_lable text_lable">
                        20% OFF Sitewide until 9/12
                    </span>
                </div>
            </div>
            <div className="nav_menu">
                <div className="container">
                    <a href={`#`} className="brand">
                        <img src={`logo.svg`} width={213}/>
                    </a>
                    <ul className="nav_page_list">
                        <li className="nav_page_item">
                            <a href="#">
                                lorem
                            </a>
                        </li> 
                        <li className="nav_page_item">
                            <a href="#">
                                consecteur
                            </a>
                        </li> 
                        <li className="nav_page_item">
                            <a href="#">
                                tempor
                            </a>
                        </li> 
                        <li className="nav_page_item">
                            <a href="#">
                                magna
                            </a>
                        </li> 
                        <li className="nav_page_item">
                            <a href="#">
                                reprehenderit
                            </a>
                        </li> 
                    </ul>
                    <ul className="nav_action_list">
                        <li className="nav_action_item">
                            <button type={`button`} className="icon_btn">
                                <img src={`search.svg`} width={16} height={16} alt={`search`}/>
                            </button>
                        </li>
                        <li className="nav_action_item">
                            <button type={`button`} className="icon_btn">
                                <img src={`account.svg`} width={16} height={16} alt={`account`}/>
                            </button>
                        </li>
                        <li className="nav_action_item">
                            <button type={`button`} className="icon_btn">
                                <img src={`bag.svg`} width={16} height={16} alt={`bag`}/>
                            </button>
                        </li>
                    </ul> 
                </div>
            </div>
        </nav>
    )
}

export default Navbar